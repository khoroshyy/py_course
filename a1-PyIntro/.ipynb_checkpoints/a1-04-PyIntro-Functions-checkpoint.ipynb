{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Python Functions\n",
    "\n",
    "A function is a block of code which only runs when it is called.\n",
    "\n",
    "You can pass data, known as parameters, into a function.\n",
    "\n",
    "A function can return data as a result."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Creating a Function\n",
    "\n",
    "In Python a function is defined using the `def` keyword:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_function():\n",
    "    print(\"Hello from a function\") "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Calling a Function\n",
    "\n",
    "To call a function, use the function name followed by parenthesis:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_function():\n",
    "    print(\"Hello from a function\")\n",
    "\n",
    "my_function()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Arguments\n",
    "\n",
    "Information can be passed into functions as arguments.\n",
    "\n",
    "Arguments are specified after the function name, inside the parentheses. You can add as many arguments as you want, just separate them with a comma.\n",
    "\n",
    "The following example has a function with one argument (surname). When the function is called, a PhD title is added after the surname and printed out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def add_PhD(surname):\n",
    "    print(surname + \", PhD\")\n",
    "\n",
    "add_PhD(\"Smith\")\n",
    "add_PhD(\"Seara\")\n",
    "add_PhD(\"Li\") "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">Arguments are often shortened to args in Python documentations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Parameters or Arguments?\n",
    "\n",
    "The terms parameter and argument can be used for the same thing: information that are passed into a function.\n",
    "\n",
    "From a function's perspective:\n",
    "\n",
    "- A parameter is the variable listed inside the parentheses in the function definition.\n",
    "- An argument is the value that is sent to the function when it is called.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "#This function expects 2 arguments, and gets 2 arguments:\n",
    "def my_function(fname, lname):\n",
    "    print(fname + \" \" + lname)\n",
    "\n",
    "my_function(\"Emil\", \"Refsnes\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "#This function expects 2 arguments, but gets only 1:\n",
    "def my_function(fname, lname):\n",
    "    print(fname + \" \" + lname)\n",
    "\n",
    "my_function(\"Emil\") "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "#This function expects 2 arguments, but 3 are given:\n",
    "def my_function(fname, lname):\n",
    "    print(fname + \" \" + lname)\n",
    "\n",
    "my_function(\"Emil\", \"Refsnes\", \"extra\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Default Parameter Value\n",
    "\n",
    "The following example shows how to use a default parameter value.\n",
    "\n",
    "If we call the function without argument, it uses the default value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_function(country = \"Norway\"):\n",
    "  print(\"I am from \" + country)\n",
    "\n",
    "my_function(\"Sweden\")\n",
    "my_function(\"India\")\n",
    "my_function()\n",
    "my_function(\"Brazil\") "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Passing a List as an Argument\n",
    "\n",
    "You can send any data types of argument to a function (string, number, list, dictionary etc.), and it will be treated as the same data type inside the function.\n",
    "\n",
    "E.g. if you send a List as an argument, it will still be a List when it reaches the function:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_function(food):\n",
    "    for x in food:\n",
    "        print(x)\n",
    "\n",
    "fruits = [\"apple\", \"banana\", \"cherry\"]\n",
    "\n",
    "my_function(fruits)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Return Values\n",
    "\n",
    "To let a function return a value, use the `return` statement:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_function(x):\n",
    "    return 5 * x\n",
    "\n",
    "print(my_function(3))\n",
    "print(my_function(5))\n",
    "print(my_function(9)) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## The pass Statement\n",
    "\n",
    "function definitions cannot be empty, but if you for some reason have a function definition with no content, put in the pass statement to avoid getting an error."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_very_nice_function_to_be_implemented_in_the_future():\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Tuples are place holders for function with multiple returns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "positions = [(0.0,21.0),\n",
    "             (2.5,13.1),\n",
    "             (33.0,1.2)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tuples can be used when functions return more than one value. Say we wanted to compute the smallest x- and y-coordinates of the above list of objects. We could write:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def mincoord(objects):\n",
    "    minx = 1.7976931348623157e+308 # These are set to really big numbers\n",
    "    miny = 1.7976931348623157e+308 # The biggest possible in Python\n",
    "    for obj in objects:\n",
    "        x,y = obj\n",
    "        if x < minx: \n",
    "            minx = x\n",
    "        if y < miny:\n",
    "            miny = y\n",
    "    return minx,miny\n",
    "x,y = mincoord(positions)\n",
    "print(x,y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we did two things with tuples you haven't seen before. First, we unpacked an object into a set of named variables using *tuple assignment*:\n",
    "\n",
    "    >>> x,y = obj\n",
    "\n",
    "We also returned multiple values (minx,miny), which were then assigned to two other variables (x,y), again by tuple assignment."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Python Documentation Strings func.\\_\\_doc\\_\\_\n",
    "In Python, a string literal is used for documenting  a module,  function, class, or method. You can access string literals by \\_\\_doc\\_\\_ (notice the double underscores) attribute of the object (e.g. my_function.\\_\\_doc\\_\\_).\n",
    "\n",
    "Docstring Conventions :\n",
    "\n",
    "- String literal literals must be enclosed with a triple quote. Docstring should be informative\n",
    "- The first line may briefly describe the object's purpose. The line should begin with a capital letter and ends with a dot.\n",
    "- If a documentation string is a muti-line string then the second line should be blank followed by any detailed explanation starting from the third line."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "See the following example with multi-line docstring:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def avg_number(x, y):\n",
    "    \"\"\"Calculate and Print Average of two Numbers.\n",
    "    \n",
    "    Created on 05/08/2020. python-docstring-example.py\n",
    "    \"\"\"\n",
    "    print(f\"Average of {x} and {y} is {(x+y)/2}\\n\")\n",
    "\n",
    "avg_number(20,10)\n",
    "\n",
    "print(avg_number.__doc__)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Alternative way to visualize __doc__ strings\n",
    "help(avg_number)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Excercises with functions "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "1. Write a Python function to sum all the numbers in a list. [Click me to see the sample solution](https://www.w3resource.com/python-exercises/python-functions-exercise-2.php)\n",
    "  - Sample List : (8, 2, 3, 0, 7)\n",
    "  - Expected Output : 20 \n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "2. Write a Python function that accepts a string and calculate the number of upper case letters and lower case letters. [Click me to see the sample solution](https://www.w3resource.com/python-exercises/python-functions-exercise-7.php)\n",
    "  - Sample String: 'The quick Brow Fox'\n",
    "  - Expected Output:\n",
    "    - No. of Upper case characters : 3\n",
    "    - No. of Lower case Characters : 12\n",
    "\n",
    "  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "3. Write a Python function that takes a list and returns a new list with unique elements of the first list. [Click me to see the sample solution](https://www.w3resource.com/python-exercises/python-functions-exercise-8.php)\n",
    "  - Sample List : [1,2,3,3,3,3,4,5]\n",
    "  - Unique List : [1, 2, 3, 4, 5]\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "4. Create a \\_\\_doc\\_\\_ string for the function of the previous exercice and visualize it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For more exercices visit https://www.w3resource.com/python-exercises/python-functions-exercises.php"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# What next?\n",
    "This is the last notebook and 4th notebook of the Python-1 course. Try to reflect on what you have learned and practice, practice, and when tired practice a bit more.\n",
    "\n",
    "Once you understand the concepts introduced here, please proceed to the [python-2 course]()."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.15"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
