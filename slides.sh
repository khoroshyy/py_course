# Python-A1 course (PyIntro-A1)
##############################
jupyter nbconvert a1-PyIntro/a1-00-PyIntro-Intro.ipynb --to slides  --SlidesExporter.reveal_scroll=True
jupyter nbconvert a1-PyIntro/a1-01-PyIntro-Datatypes.ipynb --to slides --SlidesExporter.reveal_scroll=True
jupyter nbconvert a1-PyIntro/a1-02-PyIntro-Loops_Decisions.ipynb --to slides --SlidesExporter.reveal_scroll=True 
jupyter nbconvert a1-PyIntro/a1-03-PyIntro-Syntax.ipynb --to slides --SlidesExporter.reveal_scroll=True
jupyter nbconvert a1-PyIntro/a1-04-PyIntro-Functions.ipynb --to slides --SlidesExporter.reveal_scroll=True

# Python-A2 course (PyIntro-A2)
###############################
jupyter nbconvert a2-PyIntro/a2-01-PyIntro-IO.ipynb --to slides --SlidesExporter.reveal_scroll=True
jupyter nbconvert a2-PyIntro/a2-01short-PyIntro-IO.ipynb --to slides --SlidesExporter.reveal_scroll=True
jupyter nbconvert a2-PyIntro/a2-02-PyIntro-OptArgs.ipynb --to slides --SlidesExporter.reveal_scroll=True
jupyter nbconvert a2-PyIntro/a2-03-PyIntro-FunctionsAdv.ipynb --to slides --SlidesExporter.reveal_scroll=True
jupyter nbconvert a2-PyIntro/a2-04-PyIntro-ComprehensionLists.ipynb --to slides --SlidesExporter.reveal_scroll=True
jupyter nbconvert a2-PyIntro/a2-05-PyIntro-Exceptions.ipynb --to slides --SlidesExporter.reveal_scroll=True
jupyter nbconvert a2-PyIntro/a2-06-PyIntro-Serializing.ipynb --to slides --SlidesExporter.reveal_scroll=True
jupyter nbconvert a2-PyIntro/a2-07-PyIntro-ObjectsIntro.ipynb --to slides --SlidesExporter.reveal_scroll=True
jupyter nbconvert a2-PyIntro/a2-08-PyIntro-Modules.ipynb --to slides --SlidesExporter.reveal_scroll=True


# Python-B1 course (PyInter-B1)
###############################
#jupyter nbconvert b1-PyInter/b1-PyInter-01-Assertions.ipynb --to slides

# Python-B2 course (PyInter-B2)
###############################
#jupyter nbconvert b2-PyInter/b2-01-PyInter-AdvPythonIntro.ipynb --to slides


# Python-C1 course (PyAdv-C1)
#############################


# Python-C2 course (PyAdv-C2)
#############################



